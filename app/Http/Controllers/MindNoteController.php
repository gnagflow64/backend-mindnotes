<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMindNoteRequest;
use App\Http\Requests\UpdateMindNoteRequest;
use App\Models\MindNote;
use Illuminate\Http\Response;

class MindNoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(): Response
    {
        return response([
            'message' => 'All mind notes',
            'mind_notes' => MindNote::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreMindNoteRequest  $request
     * @return Response
     */
    public function store(StoreMindNoteRequest $request): Response
    {
        $mindNote = MindNote::create($request->all());

        return response([
            'message' => "New mind note created with id: {$mindNote->id}",
            'mind_note' => $mindNote
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  MindNote  $mindnote
     * @return Response
     */
    public function show(MindNote $mindnote): Response
    {
        return response([
            'message' => "Mind note with id: {$mindnote->id}",
            'mind_note' => $mindnote
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateMindNoteRequest  $request
     * @param  MindNote  $mindnote
     * @return Response
     */
    public function update(UpdateMindNoteRequest $request, MindNote $mindnote): Response
    {

        $mindnote->update($request->all());

        return response([
            'message' => "Mind note updated {$mindnote->title}",
            'mind_note' => $mindnote->refresh()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  MindNote  $mindnote
     * @return Response
     */
    public function destroy(MindNote $mindnote): Response
    {
        $mindnote->delete();

        return response([
            'message' => "Mind note with deleted"
        ]);
    }
}
